﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace dogGo3
{
    public partial class Adopt : ContentPage
    {
        public Adopt()
        {
            InitializeComponent();
        }

        async void Handle_AdoptionsClicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new Adoptions());
        }

        async void Handle_BreedsClicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new Breeds());
        }
    }
}
