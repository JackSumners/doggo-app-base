﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Reflection;
using dogGo3.Models;
using Xamarin.Forms;

namespace dogGo3
{
    public partial class Breeds : ContentPage
    {

		ObservableCollection<BreedCells> breedAndInfo = new ObservableCollection<BreedCells>();
        public Breeds()
        {
            InitializeComponent();
            PopulateAll();
        }

        void ListDelete_Clicked(object sender, System.EventArgs e)
        {
            if (Profile.isAdmin == true)
            {
                //LocationCells info = (LocationCells)AdoptionsListView.SelectedItem;
                var menuItem = (MenuItem)sender;
                var cellInfo = (BreedCells)menuItem.CommandParameter;

                int i = 0;
                int CountL = 0;
				while (breedAndInfo[i].name != cellInfo.name)
                {
                    i++;
                    CountL++;
                }
                breedAndInfo.RemoveAt(CountL);
            }
            else { DisplayAlert("Invalid Access", "You need Admin Access to use this function!", "Go Back"); }
        }



        
		private void PopulateAll()
        {
            string text = "";
            int slash;
            //MAP ITEMS
            

			var assembly = IntrospectionExtensions.GetTypeInfo(typeof(Breeds)).Assembly;
            Stream stream = assembly.GetManifestResourceStream("dogGo3.BreedsResource.txt");

            using (var reader = new System.IO.StreamReader(stream))
            {
                text = reader.ReadLine();
                while (text != null)
                {
					string nameString, ageString, typeString, genderString, infoString, imageString;

                    if (text[0] == 'L')
                    {
                        text = text.Remove(0, 2);
                        slash = text.IndexOf('|');
                        nameString = text.Substring(0, slash);

                        text = text.Remove(0, slash + 1);
                        slash = text.IndexOf('|');
                        ageString = text.Substring(0, slash);

                        text = text.Remove(0, slash + 1);
                        slash = text.IndexOf('|');
                        typeString = text.Substring(0, slash);

						text = text.Remove(0, slash + 1);
                        slash = text.IndexOf('|');
                        genderString = text.Substring(0, slash);

						text = text.Remove(0, slash + 1);
                        slash = text.IndexOf('|');
                        infoString = text.Substring(0, slash);

                        text = text.Remove(0, slash + 1);
                        slash = text.IndexOf('|');
                        imageString = text.Substring(0, slash);

                        var stringList = new BreedCells
                        {
                            name = $"{nameString}",
                            age = $"{ageString} yrs old",
							type = $"{typeString}",
							gender = $"{genderString}",
							info = $"{typeString}",
                            itemImage = $"{imageString}"
                        };
						breedAndInfo.Add(stringList);
                    }

                    text = reader.ReadLine();
                }

				BreedsListView.ItemsSource = breedAndInfo;
            }

        }
    }
}
