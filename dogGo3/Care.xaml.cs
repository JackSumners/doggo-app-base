﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace dogGo3
{
    public partial class Care : ContentPage
    {
        public Care()
        {
            InitializeComponent();
        }

        async void Handle_SittersClicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new dogSitter());
        }

        async void Handle_MeetClicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new dogMeet());
        }

        async void Handle_GroomingClicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new dogGroom());
        }
    }
}
