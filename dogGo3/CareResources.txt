﻿G|Petco|Our Petco family consists of pet enthusiasts who support pet parents of all types of animals by ensuring that their pets are happy, loved, secure and comfortable.|https://stores.petco.com/?cm_mmc=PPC-GG-_-PTC_P_SVC_PPC-GG_FY17-NB-Dog-Grooming-EXA-_-Non-Brand_Pet-Groomers-_-43700025917816462&kwid=p25917816462&device=c&langId=-1&gclid=Cj0KCQjwibDXBRCyARIsAFHp4fpQNiM20Tp9ucZJXyZZi-Zyo4H-8xnw_JFt86d5yzKEivMPuyJccW4aAuoOEALw_wcB&gclsrc=aw.ds|g1s.jpg|
G|Murrieta Oaks Pet Hotel|Regular grooming is essential to the maintenance of your pet's health. A well groomed pet is more likely to be hugged, and pets live healthier lives when they are touched frequently and have a healthy coat and skin.|http://www.muroaksvet.com/p/veterinarian/grooming_11135/murrieta-ca-92562/murrieta-oaks-veterinary-hospital-and-pet-hotel-11135?cpm=ppcgoogle&gclid=Cj0KCQjwibDXBRCyARIsAFHp4fpDA37l6drFDMW4JwCj0A__BDSsZPGExgwMCdRtlQr4yo-ZYABVcA8aAv_eEALw_wcB|g2s.jpg|
G|PetSmart|Our professional, academy-trained, safety-certified dog & cat stylists are at your service. With our Look Great Guarantee, if your pet doesn’t look great, well do what it takes to make it right.|https://www.petsmart.com/pet-services/grooming/?gclid=Cj0KCQjwibDXBRCyARIsAFHp4frVrkaHT8G3izfDMYBXhtcCA7RJbEgAlXdTaRL7d5Fvu6uHR7IuogIaAmUsEALw_wcB|g3s.jpg|
G|Pampered Pooch Spa|Pampered Pooch Spa & Bowtique is staffed with only Certified Professional Groomers and has been serving the Murrieta Valley since March of 2009.|http://www.thepamperedpoochspa.com/|g4s.jpg|
G|Grubby Dogs Pet Wash|Grubby Dogs is one few salons that have received the American Kennel Club (AKC) Salon Certification. Pet grooming is just a reality for canines. The Grubby Dog Groomers will give your animal their own individual style, cut and trim.|http://www.grubbydogs.com/|g5s.jpg|
M|RuffTail Runners|RuffTail Runners (formerly "Jog-a-Dog") is a program brought to you by Team Spiridon (http://www.teamspiridon.org), that encourages runners to run with shelter dogs, giving the dogs a little more exercise off-campus!|https://www.meetup.com/Rufftailrunnersaustin/|s4s.jpg|
M|The Denver Doggie Happy Hour Group|Dog lovers and their canine companions are invited to join us for a variety of meetups at dog-friendly venues, from dog-friendly bars to indoor and outdoor play dates, training and education!|https://www.meetup.com/DenverDoggieHappyHourGroup/|meet1_s.jpg|
M|The San Antonio Nature Hounds|The San Antonio Nature Hounds is San Antonios finest recreational group dedicated to providing a great outdoor experience for both people and dogs.|https://www.meetup.com/sanaturehounds-com/|meet2_s.jpg|
M|Dogs & Drinks Meetup|We know your dog is your best friend, and who doesn't like to go out for a drink with their best friend? Come out and meet other dog lovers and enjoy some drinks and fun.|https://www.meetup.com/Dogs-Drinks-Meetup/|meet3_s.jpg|
M|The Balanced Dog Pack|This is a training group for those interested in working with their dogs (on leash), hiking, meeting awesome people, and having a wicked lot of fun! |https://www.meetup.com/Portland-Dog-Walking-hiking-Training-Socialization-FUN/|meet6_s.jpg|
S|Erin P.|Dog Boarding $38 in the sitter's home per night. Drop-In Visits $16 30-minute check-ins per visit. WHILE YOU'RE AT WORK, Doggy Day Care $30, in the sitter's home, per day.|https://www.rover.com/members/erin-p-dog-trainer-and-dog-lover/?service_type=overnight-boarding&start_date=&end_date=&refer=search|s1s.jpg|
S|Janean H.|Dog Boarding $42 in the sitter's home per night. House Sitting $106 in your home per night. WHILE YOU'RE AT WORK Doggy Day Care $36 in the sitter's home per day. Dog Walking $30 30-minute walks per walk.|https://www.rover.com/members/janean-h-home-full-time-multiple-play-yards/?service_type=overnight-boarding&start_date=&end_date=&refer=search|s2s.jpg|
S|Maisha C.|Dog Boarding $40 in the sitter's home per night. House Sitting $45 in your home per night. Drop-In Visits $25 30-minute check-ins per visit. WHILE YOU'RE AT WORK Doggy Day Care $40 in the sitter's home per day. Dog Walking $20 30-minute walks per walk.|https://www.rover.com/members/maisha-c-join-the-wolf-pack/?service_type=overnight-boarding&start_date=&end_date=&refer=search|s5s.jpg|
S|Jennifer T.|Dog Boarding $35 in the sitter's home per night. Drop-In Visits $20 30-minute check-ins per visit. WHILE YOU'RE AT WORK Doggy Day Care $30 in the sitter's home per day.|https://www.rover.com/members/jennifer-t-kind-caring-animal-lover/?service_type=overnight-boarding&start_date=&end_date=&refer=search|s4s.jpg|
S|Emily P.|Dog Boarding $40 in the sitter's home per night. House Sitting $25 in your home per night. Drop-In Visits $13 30-minute check-ins per visit. WHILE YOU'RE AT WORK Dog Walking $13 30-minute walks per walk.|https://www.rover.com/members/emily-p-happy-dog-happy-life/?service_type=overnight-boarding&start_date=&end_date=&refer=search|s3s.jpg|