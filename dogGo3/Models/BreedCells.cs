﻿using System;

using Xamarin.Forms;

namespace dogGo3.Models
{
    public class BreedCells : ContentPage
    {
        public string name
        {
            get;
            set;
        }

        public string age
        {
            get;
            set;
        }

		public string type
        {
            get;
            set;
        }

		public string gender
        {
            get;
            set;
        }

		public string info
        {
            get;
            set;
        }

        public ImageSource itemImage
        {
            get;
            set;
        }
    }
}

