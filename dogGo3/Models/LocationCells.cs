﻿using System;

using Xamarin.Forms;

namespace dogGo3.Models
{
    public class LocationCells : ContentPage
    {
        
        public string title
        {
            get;
            set;
        }

        public string info
        {
            get;
            set;
        }

		public string phone
        {
            get;
            set;
        }

		public string link
        {
            get;
            set;
        }

        public string email
        {
            get;
            set;
        }

    
    }
}

