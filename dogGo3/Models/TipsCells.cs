﻿using System;

using Xamarin.Forms;

namespace dogGo3.Models
{
    public class TipsCells : ContentPage
    {
       
        public string title
            {
                get;
                set;
            }

        public string link
            {
                get;
                set;
            }

		public string info
        {
            get;
            set;
        }

        public ImageSource itemImage
            {
                get;
                set;
            }
    }
}

