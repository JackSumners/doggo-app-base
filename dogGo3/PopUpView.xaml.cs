﻿using System;
using System.Collections.Generic;
using dogGo3.Models;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace dogGo3
{
	public partial class PopUpView : PopupPage
    {
		public char src;
        public PopUpView(char newsrc)
        {
            InitializeComponent();
			src = newsrc;
        }

		void Handle_Clicked(object sender, System.EventArgs e)
		{
			if (eLat.Text != null && eLon.Text != null &&
			    eName.Text != null && eAddress.Text != null && eEmail.Text != null)
			{
			    if (src == 'A')
			    {
				    Adoptions.newAddList[0] = eLat.Text;
				    Adoptions.newAddList[1] = eLon.Text;
				    Adoptions.newAddList[2] = eName.Text;
				    Adoptions.newAddList[3] = eAddress.Text;
				    Adoptions.newAddList[4] = eEmail.Text;
			    }
			    else if (src == 'V')
				{
					MapList.newMapAddList[0] = eLat.Text;
					MapList.newMapAddList[1] = eLon.Text;
					MapList.newMapAddList[2] = eName.Text;
					MapList.newMapAddList[3] = eAddress.Text;
					MapList.newMapAddList[4] = eEmail.Text;
				}

				else if (src == 'S')
                {
					StoreMap.newStoreAddList[0] = eLat.Text;
					StoreMap.newStoreAddList[1] = eLon.Text;
					StoreMap.newStoreAddList[2] = eName.Text;
					StoreMap.newStoreAddList[3] = eAddress.Text;
					StoreMap.newStoreAddList[4] = eEmail.Text;
                }

				PopupNavigation.PopAsync(true);
			}
			else{ DisplayAlert($"Invalid Submision", "Please fill out all fields to add a list!", "Ok"); }
		}

		void Handle_Cancel(object sender, System.EventArgs e)
		{
			PopupNavigation.PopAsync(true);
		}
    }
}
