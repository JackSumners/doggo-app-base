﻿using System;
using System.Collections.Generic;
using dogGo3.Models;
using Xamarin.Forms;

namespace dogGo3
{
    public partial class Profile : ContentPage
    {
        //CREATES A PUBLIC VARIABLE USED THROUGHOUT THE APP TO VERIFY ADMIN ACCESS
        public static bool isAdmin = false;

        public List<string> userN = new List<string>();
        public List<string> passW = new List<string>();

        public Profile()
        {
            InitializeComponent();
            userN.Add("Jack");
            passW.Add("12345");
        }

        void Handle_LoginClicked(object sender, System.EventArgs e)
        {
            if (userEntry.Text == userN[0] && passwordEntry.Text == passW[0])
            {
                isAdmin = true;
                UserLabel.Text = "Welcome Admin";
                DisplayAlert("Welcome Admin", "You now have access to Admin Level functions! You can now find new functions at the bottom of this page!", "Ok");

            }
            else
            {
                isAdmin = false;
                UserLabel.Text = $"Welcome {userEntry.Text}";
                DisplayAlert($"Welcome {userEntry.Text}", "We hope you enjoy our app!", "Ok");
            }
        }

        
    }
}
