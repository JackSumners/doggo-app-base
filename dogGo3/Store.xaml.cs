﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace dogGo3
{
    public partial class Store : ContentPage
    {
        public Store()
        {
            InitializeComponent();
        }

        async void Handle_StoresClicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new StoreMap());
        }
    }
}