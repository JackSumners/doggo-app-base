﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using dogGo3.Models;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;
using Plugin.Geolocator;
using System.Reflection;
using System.IO;
using System.Diagnostics;
using Rg.Plugins.Popup.Services;

namespace dogGo3
{
    [XamlCompilation(XamlCompilationOptions.Compile)]

    public partial class StoreMap : ContentPage
    {

		ObservableCollection<LocationCells> StoreLocationInfo = new ObservableCollection<LocationCells>();

		public static object Pins { get; private set; }

		public static List<string> newStoreAddList = new List<string>() { "", "", "", "", "" };

		public StoreMap()
        {
            InitializeComponent();

			var searchItem = new ToolbarItem()
            {
                Text = "Add Info",
                Command = new Command(() => PopUpAccess())
            };
            ToolbarItems.Add(searchItem);

            var initialMapLocation = MapSpan.FromCenterAndRadius(new Position(33.12974, -117.1586638), Distance.FromMiles(8));
            StoresMap.MoveToRegion(initialMapLocation);

			PopulateAll();

        }


        void PopUpAccess()
        {
            if (Profile.isAdmin)
            {
                PopupNavigation.PushAsync(new PopUpView('S'));
            }
            else { DisplayAlert("Invalid Access", "You need Admin Access to use this function!", "Go Back"); }
        }


		void Handle_Refreshing(object sender, System.EventArgs e)
		{
			if (newStoreAddList[0] != "")
            {
                string recentTitle = $"{newStoreAddList?[2]}";
                if (StoreLocationInfo[(StoreLocationInfo.Count - 1)].title != recentTitle)
                {
                    double tLat = Convert.ToDouble(newStoreAddList?[0]);
                    double tLon = Convert.ToDouble(newStoreAddList?[1]);
                    var stringPin = new Pin
                    {
                        Type = PinType.Place,
                        Position = new Position(tLat, tLon), //latitude & longitude
                        Label = $"{newStoreAddList?[2]}",
						Address = $"{newStoreAddList?[3]}"
                    };
                    StoresMap.Pins.Add(stringPin);

                    var stringList = new LocationCells
                    {
						title = $"{newStoreAddList?[2]}",
                        info = $"{newStoreAddList?[3]}",
						email = $"{newStoreAddList?[4]}"
                    };
                    StoreLocationInfo.Add(stringList);
                    StoresListView.ItemsSource = StoreLocationInfo;

                }
            }
            StoresListView.IsRefreshing = false;
		}

		void Handle_LinkClicked(object sender, System.EventArgs e)
		{
			Button InfoB = (Button)sender;    //button starts event
			var uri = new Uri(InfoB.BindingContext.ToString());
            Device.OpenUri(uri);
		}

        async void Contact_Clicked(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var cellSelected = (LocationCells)menuItem.CommandParameter;
            string contactEmail = cellSelected.email;
            await Navigation.PushAsync(new EmailPage(contactEmail));
        }




        private async void Location_Clicked(object sender, System.EventArgs e)
        {
            var locator = CrossGeolocator.Current;
            locator.DesiredAccuracy = 15;
            var locations = await locator.GetPositionAsync(TimeSpan.FromMilliseconds(10000));
            Position position = new Position(locations.Latitude, locations.Longitude);
            StoresMap.MoveToRegion(MapSpan.FromCenterAndRadius(position, Distance.FromMiles(100)));
        }

        void Delete_Clicked(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            if (Profile.isAdmin == true)
            {
                //LocationCells info = (LocationCells)AdoptionsListView.SelectedItem;
                var menuItem = (MenuItem)sender;
                var cellInfo = (LocationCells)menuItem.CommandParameter;

                int i = 0;
                int CountL = 0;
                while (StoreLocationInfo[i].title != cellInfo.title)
                {
                    i++;
                    CountL++;
                }
                StoreLocationInfo.RemoveAt(CountL);
                StoresMap.Pins.RemoveAt(CountL);
            }
            else { DisplayAlert("Invalid Access", "You need Admin Access to use this function!", "Go Back"); }
        }


		private void PopulateAll()
        {
			
            string text = "";
            int slash;
            //MAP ITEMS
            string latitudeString, longitudeString;
            double latitude, longitude;
            string labelString;
            string infoPinString;
            //LIST ITEMS
            string titleString, infoListString, emailString, phoneString, linkString;

			var assembly = IntrospectionExtensions.GetTypeInfo(typeof(StoreMap)).Assembly;
            Stream stream = assembly.GetManifestResourceStream("dogGo3.StoreResources.txt");

            using (var reader = new System.IO.StreamReader(stream))
            {
                text = reader.ReadLine();
                while (text != null)
                {

                    if (text[0] == 'P')
                    {
                        text = text.Remove(0, 2);
                        slash = text.IndexOf('|');
                        latitudeString = text.Substring(0, slash);
                        latitude = Convert.ToDouble(latitudeString);

                        text = text.Remove(0, slash + 1);
                        slash = text.IndexOf('|');
                        longitudeString = text.Substring(0, slash);
                        longitude = Convert.ToDouble(longitudeString);

                        text = text.Remove(0, slash + 1);
                        slash = text.IndexOf('|');
                        labelString = text.Substring(0, slash);

                        text = text.Remove(0, slash + 1);
                        slash = text.IndexOf('|');
                        infoPinString = text.Substring(0, slash);

                        //text = text.Remove(0, slash);

                        var stringPin = new Pin
                        {
                            Type = PinType.Place,
                            Position = new Position(latitude, longitude), //latitude & longitude
                            Label = $"{labelString}",
                            Address = $"{infoPinString}"
                        };

                        StoresMap.Pins.Add(stringPin);
                    }
                    else if (text[0] == 'L')
                    {
                        text = text.Remove(0, 2);
                        slash = text.IndexOf('|');
                        titleString = text.Substring(0, slash);

                        text = text.Remove(0, slash + 1);
                        slash = text.IndexOf('|');
                        infoListString = text.Substring(0, slash);

						text = text.Remove(0, slash + 1);
                        slash = text.IndexOf('|');
                        linkString = text.Substring(0, slash);

						text = text.Remove(0, slash + 1);
                        slash = text.IndexOf('|');
                        phoneString = text.Substring(0, slash);

                        text = text.Remove(0, slash + 1);
                        slash = text.IndexOf('|');
                        emailString = text.Substring(0, slash);

						var stringList = new LocationCells
                        {
                            title = $"{titleString}",
                            info = $"{infoListString}",
							link = $"{linkString}",
							phone = $"{phoneString}",
                            email = $"{emailString}",
                        };
                        StoreLocationInfo.Add(stringList);
                    }

                    text = reader.ReadLine();
                }

                StoresListView.ItemsSource = StoreLocationInfo;
            }

        }


        //EXCEPTION HANDLING FOR LIST CLICKS AND UPDATED MAP LOCATION
        // ***** MAYBE WE CAN INTEGRATE 1 MAP FOR ALL MAP LISTS???(very tough) *****
        void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var info = (ListView)sender;
            var selected = (LocationCells)info.SelectedItem;

            int i = 0;
            int CountL = 0;
            while (StoreLocationInfo[i].title != selected.title)
            {
                i++;
                CountL++;
            }

            var NewMapLocation = MapSpan.FromCenterAndRadius((StoresMap.Pins[i].Position)
                                        , Distance.FromMiles(8));
            StoresMap.MoveToRegion(NewMapLocation);
        }
    }
}