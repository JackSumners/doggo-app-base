﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Reflection;
using dogGo3.Models;
using Xamarin.Forms;

namespace dogGo3
{
    public partial class TipsList : ContentPage
    {

		ObservableCollection<TipsCells> tipsAndInfo = new ObservableCollection<TipsCells>();
        public TipsList()
        {
            InitializeComponent();
			PopulateAll();
        }

        void Handle_Clicked(object sender, System.EventArgs e)
        {

            Button InfoB = (Button)sender;    //button starts event
			var uri = new Uri(InfoB.BindingContext.ToString());
            Device.OpenUri(uri);

        }


        void Delete_Clicked(object sender, System.EventArgs e)
        {
            if (Profile.isAdmin == true)
            {
                //LocationCells info = (LocationCells)AdoptionsListView.SelectedItem;
                var menuItem = (MenuItem)sender;
                var cellInfo = (TipsCells)menuItem.CommandParameter;

                int i = 0;
                int CountL = 0;
                while (tipsAndInfo[i].title != cellInfo.title)
                {
                    i++;
                    CountL++;
                }
                tipsAndInfo.RemoveAt(CountL);

            }
            else { DisplayAlert("Invalid Access", "You need Admin Access to use this function!", "Go Back"); }
        }

        
		private void PopulateAll()
        {
            string text = "";
            int slash;
            //MAP ITEMS


			var assembly = IntrospectionExtensions.GetTypeInfo(typeof(TipsList)).Assembly;
            Stream stream = assembly.GetManifestResourceStream("dogGo3.TipsResources.txt");

            using (var reader = new System.IO.StreamReader(stream))
            {
                text = reader.ReadLine();
                while (text != null)
                {
                    string titleString, infoString, linkString;

                    if (text[0] == 'L')
                    {
                        text = text.Remove(0, 2);
                        slash = text.IndexOf('|');
                        titleString = text.Substring(0, slash);

                        text = text.Remove(0, slash + 1);
                        slash = text.IndexOf('|');
                        infoString = text.Substring(0, slash);

                        text = text.Remove(0, slash + 1);
                        slash = text.IndexOf('|');
                        linkString = text.Substring(0, slash);

                        var stringList = new TipsCells
                        {
							title = $"{titleString}",
							info = $"{infoString}",
							link = $"{linkString}",
                            itemImage = "tips.png"
                        };
                        tipsAndInfo.Add(stringList);
                    }

                    text = reader.ReadLine();
                }

                TipsListView.ItemsSource = tipsAndInfo;
            }

        }
    }
}

