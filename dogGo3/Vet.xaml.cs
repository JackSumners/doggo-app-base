﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace dogGo3
{
    public partial class Vet : ContentPage 
    {
        public Vet()
        {
            InitializeComponent();
        }

        async void Handle_TipsClicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new TipsList());
        }

        async void Handle_LocationsClicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new MapList());
        }
    }
}
