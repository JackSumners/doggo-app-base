﻿using System;
using System.Collections.Generic;
using System.Text;
using dogGo3.Models;

namespace dogGo3.ViewModel
{
    public class ViewMeet
    {
        public List<Meet> Cells { get; set; }

        public ViewMeet()
        {
            Cells = new Meet().GetCellItem();
        }
    }
}
