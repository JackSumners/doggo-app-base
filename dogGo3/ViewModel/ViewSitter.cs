﻿using System;
using System.Collections.Generic;
using System.Text;
using dogGo3.Models;

namespace dogGo3.ViewModel
{
    public class ViewSitter
    {
        public List<Sitter> Cells { get; set; }

        public ViewSitter()
        {
            Cells = new Sitter().GetCellItem();
        }
    }
}
