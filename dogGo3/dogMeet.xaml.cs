﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using dogGo3.Models;
using dogGo3.ViewModel;
using System.Reflection;
using System.IO;

namespace dogGo3
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class dogMeet : ContentPage
    {
		ObservableCollection<CareCells> MeetAndInfo = new ObservableCollection<CareCells>();
        public dogMeet()
        {
            InitializeComponent();
			PopulateAll();
        }


        void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var listView = (ListView)sender;
            CareCells itemTapped = (CareCells)listView.SelectedItem;

			for (int i = 0; i < MeetAndInfo.Count; i++)
            {
                if (itemTapped.title == MeetAndInfo[i].title)
                {
                    var uri = new Uri(MeetAndInfo[i].link);
                    Device.OpenUri(uri);
                }
            }

        }

        void ListDelete_Clicked(object sender, System.EventArgs e)
        {
            if (Profile.isAdmin == true)
            {
                //LocationCells info = (LocationCells)AdoptionsListView.SelectedItem;
                var menuItem = (MenuItem)sender;
                var cellInfo = (CareCells)menuItem.CommandParameter;

                int i = 0;
                int CountL = 0;
                while (MeetAndInfo[i].title != cellInfo.title)
                {
                    i++;
                    CountL++;
                }
                MeetAndInfo.RemoveAt(CountL);
            }
            else { DisplayAlert("Invalid Access", "You need Admin Access to use this function!", "Go Back"); }
        }




        private void PopulateAll()
        {
            string text = "";
            int slash;
            //MAP ITEMS


			var assembly = IntrospectionExtensions.GetTypeInfo(typeof(dogMeet)).Assembly;
            Stream stream = assembly.GetManifestResourceStream("dogGo3.CareResources.txt");

            using (var reader = new System.IO.StreamReader(stream))
            {
                text = reader.ReadLine();
                while (text != null)
                {
                    string titleString, infoString, linkString, imageString;

                    if (text[0] == 'M')
                    {
                        text = text.Remove(0, 2);
                        slash = text.IndexOf('|');
                        titleString = text.Substring(0, slash);

                        text = text.Remove(0, slash + 1);
                        slash = text.IndexOf('|');
                        infoString = text.Substring(0, slash);

                        text = text.Remove(0, slash + 1);
                        slash = text.IndexOf('|');
                        linkString = text.Substring(0, slash);

                        text = text.Remove(0, slash + 1);
                        slash = text.IndexOf('|');
                        imageString = text.Substring(0, slash);


                        var stringList = new CareCells
                        {
                            title = $"{titleString}",
                            info = $"{infoString}",
                            link = $"{linkString}",
                            itemImage = $"{imageString}"

                        };
                        MeetAndInfo.Add(stringList);
                    }
                    
                    text = reader.ReadLine();
                }

				dogMeetCells.ItemsSource = MeetAndInfo;
            }

        }
    }
}
