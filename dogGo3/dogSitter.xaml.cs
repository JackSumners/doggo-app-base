﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using dogGo3.Models;
using dogGo3.ViewModel;
using System.Reflection;
using System.IO;

namespace dogGo3
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class dogSitter : ContentPage
    {
		ObservableCollection<CareCells> SitterAndInfo = new ObservableCollection<CareCells>();
        public dogSitter()
        {
            InitializeComponent();
			PopulateAll();
        }


        void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var listView = (ListView)sender;
            CareCells itemTapped = (CareCells)listView.SelectedItem;

			for (int i = 0; i < SitterAndInfo.Count; i++)
            {
				if (itemTapped.title == SitterAndInfo[i].title)
                {
					var uri = new Uri(SitterAndInfo[i].link);
                    Device.OpenUri(uri);
                }
            }

        }

        void ListDelete_Clicked(object sender, System.EventArgs e)
        {
            if (Profile.isAdmin == true)
            {
                //LocationCells info = (LocationCells)AdoptionsListView.SelectedItem;
                var menuItem = (MenuItem)sender;
                var cellInfo = (CareCells)menuItem.CommandParameter;

                int i = 0;
                int CountL = 0;
				while (SitterAndInfo[i].title != cellInfo.title)
                {
                    i++;
                    CountL++;
                }
				SitterAndInfo.RemoveAt(CountL);
            }
            else { DisplayAlert("Invalid Access", "You need Admin Access to use this function!", "Go Back"); }
        }




        private void PopulateAll()
        {
            string text = "";
            int slash;
            //MAP ITEMS


            var assembly = IntrospectionExtensions.GetTypeInfo(typeof(Breeds)).Assembly;
            Stream stream = assembly.GetManifestResourceStream("dogGo3.CareResources.txt");

            using (var reader = new System.IO.StreamReader(stream))
            {
                text = reader.ReadLine();
                while (text != null)
                {
					string titleString, infoString, linkString, imageString;

                    if (text[0] == 'S')
                    {
                        text = text.Remove(0, 2);
                        slash = text.IndexOf('|');
                        titleString = text.Substring(0, slash);

                        text = text.Remove(0, slash + 1);
                        slash = text.IndexOf('|');
                        infoString = text.Substring(0, slash);

                        text = text.Remove(0, slash + 1);
                        slash = text.IndexOf('|');
                        linkString = text.Substring(0, slash);

						text = text.Remove(0, slash + 1);
                        slash = text.IndexOf('|');
                        imageString = text.Substring(0, slash);

						var stringList = new CareCells
						{
							title = $"{titleString}",
							info = $"{infoString}",
							link = $"{linkString}",
							itemImage = $"{imageString}"
						};
						SitterAndInfo.Add(stringList);
                    }

                    text = reader.ReadLine();
                }

				dogSitterCells.ItemsSource = SitterAndInfo;
            }

        }
    }
}
